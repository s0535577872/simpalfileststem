// this computes 1 / sqrt(number)

#define Approximation(y) (y * ( 1.5F - ( (number * 0.5F) * y * y ) ))
#define Hexadecimal 0x5f3759df 


float Q_rsqrt( float number )
{
        long i;
        float  y;
        y  = number;
        i  = * ( long * ) &y;                      
        i  = Hexadecimal - ( i >> 1 );       //WTF       
        y  = * ( float * ) &i;
        y  = Approximation(y);
 
        return y;
}
